<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;
use Session;

class ThreadController extends Controller
{
    function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        $threads=Thread::paginate(15);

        return view('thread.index',compact('threads'));
    }


    public function create()
    {
      return view('thread.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'subject'=>'required|min:5',
            'type'=>'required',
            'thread'=>'required|min:20'
            ]);
        auth()->user()->threads()->create($request->all());
       // Thread::create($request->all());//I so ova metoda mozi da zacuvuvas
        Session::flash('success','Created with sucess');
        return redirect()->back();
    }

    public function show(Thread $thread)
    {
        return view('thread.single',compact('thread'));
    }


    public function edit(Thread $thread)
    {
        if(auth()->user()->id !== $thread->user_id)
        {
            return abort('403');
        }
        return view('thread.edit',compact('thread'));
    }


    public function update(Request $request,Thread $thread)
    {
        if(auth()->user()->id !== $thread->user_id)
        {
            return abort('403');
        }
        $this->validate($request,[
            'subject'=>'required|min:5',
            'type'=>'required',
            'thread'=>'required|min:20'
        ]);

        $thread->update($request->all());

        Session::flash('success','Edit with sucess');
        return redirect()->route('thread.show',$thread->id);
    }

    public function destroy(Thread $thread)
    {
        if(auth()->user()->id !== $thread->user_id)
        {
            return abort('403');
        }
        $thread->delete();
        Session::flash('success','Delete with sucess');
        return redirect()->route('thread.index');
    }
}
