<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Petar Forum</title>

    <link href="{{asset('add/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/main.css')}}" rel="stylesheet"/>

</head>
<body>
@include('layouts.partials.nav')

@yield('banner')

<div class="container">

    <div class="row">
        @section('category')
            @include('layouts.partials.categories')
        @show
        <div class="col-md-9">
            <div class="row content-heading">
               <h4>@yield('heading')</h4>
            </div>
            <div class="content-wrap well">
                @yield('content')
            </div>
        </div>
    </div>
</div>
<script
    src="https://code.jquery.com/jquery-3.1.0.js"
    integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
    crossorigin="anonymous"></script>

</body>
</html>
