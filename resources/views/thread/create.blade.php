@extends('layouts.front')

@section('heading',"Create Thread")

@section('content')

   @include('layouts.partials.error')

   @include('layouts.partials.success')


    <div class="well">
        <form class="form-vertical" action="{{route('thread.store')}}" method="Post" role="form"
              id="create-thread-form">
            {{csrf_field()}}

            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" name="subject" placeholder="Input" value="{{old('subject')}}">
            </div>

            <div class="form-group">
                <label for="subject">Type</label>
                <input type="text" class="form-control" name="type" placeholder="Input" value="{{old('type')}}">
            </div>

            <div class="form-group">
                <label for="subject">Thread</label>
                <textarea type="text" class="form-control" name="thread" placeholder="Input" value="{{old('thread')}}">
                </textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
