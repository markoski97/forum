@extends('layouts.front')
@section('heading')
    <a class="btn btn-primary pull-right" href="{{route('thread.create')}}">Create Threads</a>
@endsection
@section('content')


    @include('thread.partials.thread-list')
@endsection
