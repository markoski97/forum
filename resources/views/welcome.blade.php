@extends('layouts.front')

@section('banner')
    <div class="jumbotron">
        <div class="container">
            <h1>Forum Petar Join</h1>
            <p>Ask questions and Help Others</p>

        </div>
    </div>
@endsection

@section('heading',"Threads")


@section('content')
    @include('thread.partials.thread-list')
@endsection
